# README #

Hello and welcome to the developer test. We would like you to create a post slider carousel plugin for WP. 
This will be a carousel that displays up to 5 posts. Each slide should dislay an image, post title, text snippet and a link to the blog post.

### Resources ###
Here are some resources for your test, you don't have to use these, they're their for your convenience.

* [Advanced Custom Fields](https://en-gb.wordpress.org/plugins/advanced-custom-fields/)
* [Slick Slider](https://kenwheeler.github.io/slick/)

------

### Rules ###

* Use any resource available to you.
* Push all your changes back to this repository.
* Take no longer than 2 hrs. (I don't expect this to be 100% complete).

------

### What is this repository for? ###

* This test is for me to better understand how you work.

------

#### What should you add to your commits? ###

* Please share any links to the online resources you've used.
* Please share links to stackoverflow if used.

------

Good luck!

