<?php
/**
 * Plugin Name: eLab WP Carousel
 * Plugin URI:  https://bitbucket.org/aaronelab/elab-wp-carousel
 * Description: eLabs carousel test plugin
 * Version:     1.0.0.0
 * Author:      
 * Text Domain: 
 * Domain Path: /languages
 */



add_filter('acf/settings/load_json', 'my_acf_json_load_point');
function my_acf_json_load_point( $paths ) {
    // append path
    $paths[] = plugin_dir_path( __FILE__ ) . '/acf-json';
    // return
    return $paths;
}

add_filter('acf/settings/save_json', 'my_acf_json_save_point');
function my_acf_json_save_point( $path ) {
    // update path
    $path = plugin_dir_path( __FILE__ ) . '/acf-json';
    // return
    return $path;    
}